#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	if (argc<2)
	{
		return 1;
	}
	
	else	{
		/*posicao zero e atm, posicao 1 e o valor desejado*/
		int pedido=atoi(argv[1]); 
		int hidrogenio, helio, gravidade, gas;

		gas=pedido/50;
		pedido=pedido-(50*gas);

		gravidade=pedido/10;
		pedido=pedido-(10*gravidade);

		helio=pedido/5;
		pedido=pedido-(5*helio);

		hidrogenio=pedido;

		printf("%d hidrogenio\n", hidrogenio);	
		printf("%d helio\n", helio);
		printf("%d gravidade\n", gravidade);
		printf("%d gas\n", gas);

		return 0;
	}
}

